require('newrelic');
require('dotenv').config();
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var jwt= require('jsonwebtoken');

const Token= require('./models/token')
const Usuario=require('./models/usuarios');

const passport= require('./config/passport');
const session= require('express-session');
const MongoDBStore= require('connect-mongodb-session')(session);
var indexRouter = require('./routes/index');
var usuariosRouter = require('./routes/usuarios');
var tokenRouter= require('./routes/token');
var bicicletasRouter = require('./routes/bicicletas');
var bicicletasApiRouter = require('./routes/api/bicicletas');
var usuarioApiRouter=require('./routes/api/usuarios');
var authRouter=require('./routes/api/auth');


let store;
if(process.env.NODE_ENV ==='develpment'){
  store=new session.MemoryStore;
}else{
  store=new MongoDBStore({
    uri:process.env.MONGO_URI,
    collection:'sessions'
  });
  store.on('error',function (error) {
    assert.ifError(error);
    assert.ok(false);
  });
};



var app = express();
app.set('secretKey','jwt_*853211*')
app.use(session({
cookie:{maxAge: 240 * 60 * 60 * 1000},
store:store,
saveUninitialized:true,
resave:'true',
secret:'red_bicis_!!!**!!**!!**"!""12345'

}));

var mongoose=require('mongoose');
const { assert } = require('console');
//var mongoDB='mongodb://localhost/red_bicicletas'
//mongodb+srv://admin:<password>@red-bicicletas.6h75f.mongodb.net/<dbname>?retryWrites=true&w=majority

var mongoDB=process.env.MONGO_URI;
mongoose.connect(mongoDB,{ useNewUrlParser: true,useUnifiedTopology: true,useCreateIndex:true,useFindAndModify:false });
mongoose.Promise=global.Promise;
var db=mongoose.connection;

db.on('error',console.error.bind(console,'connection error.'));
db.once('open',function () {
  console.log('conectado a mongo');
  
});


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/login',function (req,res) {
  res.render('session/login')
});
app.post('/login',function (req,res, next) {
  passport.authenticate('local', function (err, usuario, info) {
    if(err) return next(err);
    if(!usuario) return res.render('session/login',{info});
    req.logIn(usuario, function (err) {
      if(err) return next(err);
      return res.redirect('/');
    });
  })(req, res, next);
});
app.get('/logout',function (req,res) {
  req.logout();
  res.redirect('/login');
});
app.get('/forgotPassword',function (req,res) {
  res.render('session/forgotPassword');
});
app.post('/forgotPassword',function (req,res,next) {
  
 Usuario.findOne({email:req.body.email}, function (err, usuario) {
   if (!usuario) return res.render('session/forgotPassword',{info:{message:'no existe el email para un usuario existente.'}});
   usuario.resetPassword(function (err) {
     if(err) return next(err);
     console.log('session/forgotPasswordMessage');
   });
   res.render('session/forgotPasswordMessage');
 });

});

app.get('/resetPassword/:token', function (req, res, next) {
   
  Token.findOne({ token: req.params.token}, function (err, token) {  
    if(!token) return res.status(400).send({type: 'no-verified',msg:'no existe un usuario asociado al token.Verifique que su token no halla expirado.'});
    
    Usuario.findById(token._userId, function (err, usuario) {
      if(!usuario) return res.status(400).send({msg: 'no existe un usuario asociado a este token.'});
      res.render('session/resetPassword', {errors: {},usuario:usuario});
    });
  });
});

app.post('/resetPassword',function (req, res) {
 
  if(req.body.password != req.body.confirm_password) {
   res.render('session/resetPassword', {errors:{confirm_password:{message:'no coincide con el password ingresado'}},usuario:new Usuario({email:req.body.email})});
   return;
 }
  Usuario.findOne({email:req.body.email},function (err, usuario) {
  usuario.password=req.body.password;
  usuario.save(function (err) {
    if(err){
      res.render('session/resetPassword',{erros: err.errors, usuario: new Usuario({email:req.body.email})});
    } else{
      res.redirect('/login');
    }});
  });
});

app.use('/', indexRouter);
app.use('/usuarios', usuariosRouter);
app.use('/token', tokenRouter);
app.use('/bicicletas',loggedIn ,bicicletasRouter);
app.use('/api/bicicletas',validarUsuario ,bicicletasApiRouter);
app.use('/api/usuarios', usuarioApiRouter);
app.use('/api/auth', authRouter)

app.use('/privacy_policy', function (req, res) {
  res.sendFile('public/privacy_policy.html' );
});

app.use('/google30cb74daae5b496a', function (req, res) {
  res.sendFile('public/google30cb74daae5b496a.html' );
});

app.get('/auth/google',
  passport.authenticate('google', { scope: ['profile','email'] }));

app.get('/auth/google/callback', 
  passport.authenticate('google', { failureRedirect: '/error' }),
  function(req, res) {
    // Successful authentication, redirect home.
    res.redirect('/');
  });

app.get('/auth/facebook',
  passport.authenticate('facebook',{
    scope: "email"
  }));

app.get('/auth/facebook/callback',
  passport.authenticate('facebook', { failureRedirect: '/error' }),
  function(req, res) {
    // Successful authentication, redirect home.
    res.redirect('/');
  });

// catch 404 and forward to error handler

app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req,res,next) {
 
  if(req.user){
    next();
  }else{
    console.log('usuario sin logarse');
    
    res.redirect('/login');
  }
}


function validarUsuario(req, res, next) {
jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function (err, decoded) {
  if (err) {
    res.json({status:"error", message: err.message, data:null})
  }
  else{
    req.body.userId=decoded.id;

    console.log('jwt verify: '+ decoded);

    next();
  }
});  
}

module.exports = app;
