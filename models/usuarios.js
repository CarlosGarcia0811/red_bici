var mongoose  = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator'); 
var Reserva= require('./reserva');
const bcrypt= require('bcrypt');
const crypto=require('crypto');
const saltRounds=10;

const Token = require('../models/token');
const mailer = require('../mailer/mailer');

var Schema= mongoose.Schema;

const validateEmail= function (email) {
    const re =/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z]{2,3})+$/;
    return re.test(email);
}

var usuarioSchema=new Schema ({
    nombre:{
        type:String,
        trim:true,
        required:[true, 'el nombre es obligatorio']
    },
    email:{
        type:String,
        trim:true,
        required:[true, 'el email es obligatorio'],
        lowercase:true,
        unique:true,
        validate: [validateEmail,'por favor ingrese un email valido'],
        match:[/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z]{2,3})+$/]
    },
    password:{
        type: String,
        required:[true, 'el password obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado:{
        type:Boolean,
        default:false
    },
    googleId:String,
    facebookId:String
});

usuarioSchema.plugin(uniqueValidator, {message:'El {PATH} ya existe con otro usuario'});

usuarioSchema.pre('save', function (next) {
    if(this.isModified('password')){
        this.password=bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});
usuarioSchema.methods.validPassword=function (password) {
        return bcrypt.compareSync(password, this.password);
}

usuarioSchema.methods.reservar=function (biciId,desde,hasta, cb) {
    var reserva= new Reserva({usuario:this._id, bicicleta:biciId, desde:desde, hasta:hasta});
console.log(reserva);
reserva.save(cb);
};

usuarioSchema.methods.enviar_email_bienvenida=function (cb) {
    const token = new Token({_userId:this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination=this.email;
    
    token.save(function (err) {
        if (err){return console.log(err.message);}
        
        let mailOptions;
        if (process.env.NODE_ENV === 'production') {
                mailOptions={
                from:'carlosgustavo0811@hotmail.com',
                to: email_destination,
                subject:'verificacion de cuenta',
                text:'Hola,\n\n'+'por favor verificar su cuenta haga click en el este link:\n'+'https://red-bici.herokuapp.com'+'\/token/confirmation\/'+ token.token + '\n'
            }       
        }else{
                       mailOptions={
            from:'carlosgustavo0811@hotmail.com',
            to: email_destination,
            subject:'verificacion de cuenta',
            text:'Hola,\n\n'+'por favor verificar su cuenta haga click en el este link:\n'+'http://localhost:3000'+'\/token/confirmation\/'+ token.token + '\n'
            
        };
       }
        mailer.sendMail(mailOptions, function (err) {
            if(err){ return console.log(err.message);}

            console.log('se a enviado un mensaje de bienvenida a:'+ email_destination+'.');
        });
    });
}
usuarioSchema.methods.resetPassword=function (cb) {
    const token = new Token({_userId:this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination=this.email;
    
    token.save(function (err) {
        if (err){return cb(err);}

        let mailOptions;
        if (process.env.NODE_ENV === 'production') {
            
            mailOptions={
                from:'carlosgustavo0811@hotmail.com',
                to: email_destination,
                subject:'verificacion de cuenta',
                text:'Hola,\n\n'+'por favor resetear el password de su cuenta haga click en el este link:\n'+'https://red-bici.herokuapp.com'+'\/resetPassword\/'+ token.token + '\n'
                        
            };
        } else {
            if (process.env.NODE_ENV === 'production') {
            mailOptions={
                from:'carlosgustavo0811@hotmail.com',
                to: email_destination,
                subject:'verificacion de cuenta',
                text:'Hola,\n\n'+'por favor resetear el password de su cuenta haga click en el este link:\n'+'http://localhost:3000'+'\/resetPassword\/'+ token.token + '\n'
                        
            };
        }
        }

        mailer.sendMail(mailOptions, function (err) {
            if(err){ return cb(err);}

            console.log('se a enviado un email para resetear el password a:'+ email_destination+'.');
        });
        cb(null);
    });
}
usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition,callback){
    const self = this;
    console.log(condition);
    self.findOne({
        $or:[{'googleId': condition.id},{'email':condition.emails[0].value}
    ]},(err,result) =>{
        if(result){
            callback(err,result);
        }else{
            console.log('------------- CONDITION --------------');
            console.log(condition);
            let values = {};
            values.googleId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName || 'SIN NOMBRE';
            values.verificado = true;
            values.password = condition._json.sub;
            console.log('-------------- VALUES ---------------');
            console.log(values);
            self.create(values,(err,result) =>{
                if(err) {console.log(err);}
                return callback(err,result);
            });
        }
    });
}

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition,callback){
    const self = this;
    console.log(condition);
    self.findOne({
        $or:[{'facebookId': condition.id},{'email':condition.emails[0].value}
    ]},(err,result) =>{
        if(result){
            callback(err,result);
        }else{
            console.log('------------- CONDITION --------------');
            console.log(condition);
            let values = {};
            console.log(condition.email);
            values.facebookId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName || 'SIN NOMBRE';
            values.verificado = true;
            values.password = crypto.randomBytes(16).toString('hex');
            console.log('-------------- VALUES ---------------');
            console.log(values);
            self.create(values,(err,result) =>{
                if(err) {console.log(err);}
                return callback(err,result);
            });
        }
    });
}


module.exports=mongoose.model('Usuario',usuarioSchema);